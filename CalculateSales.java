package jp.alhinc.iyama_shogo.calculate_sales;//パッケージ名

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales{//クラス名

	public static void main(String [] args){


		BufferedReader br = null;//支店定義ファイル
		BufferedReader br2 = null;//売上ファイル

		Map<String,String> map1 = new HashMap<>();
		Map<String,Long> map2 = new HashMap<>();


		File files = new File(args[0]);//フィルター
		FilenameFilter filter = new FilenameFilter(){
			public boolean accept(File dir, String name ){
				if (name.matches("^[0-9]{8}.rcd$")){
					return true;
				}else{
					return false;
				}
			}
		};

		try{
			//例外が発生する可能性のある処理

			File file = new File (args[0],"branch.lst");//引数1にファイルがある場所、引数2にファイル名を指定する

			if (! file.exists()) //ファイルが存在しない場合(if文を使う)
			{
				System.out.println ("支店定義ファイルが存在しません"); //コンソールに出力する
				return;
			}
			FileReader fr = new FileReader(file);//ファイルリーダでファイルの読み込み
			br = new BufferedReader(fr);


			String line;//Stringは文字列
			while((line = br.readLine()) != null)
			{//readLineで1行ずつ読み込む
				String[] str = line.split(",");

				if (! str[0].matches("^[0-9]{3}$") || str.length != 2) //フォーマットが不正の場合(if文を使う)
				{
					System.out.println ("支店定義ファイルのフォーマットが不正です"); //コンソールに出力する
					return;
				}


				map1.put(str[0],str[1]);//支店コードと支店名をMapに入れ込む
				map2.put(str[0], 0L);//支店コードと金額

			}
		}
		catch(IOException e) {//例外が発生した場合の処理(Exceptionクラスを指定)
			System.out.println("予期せぬエラーが発生しました");

		}
		finally{
			//例外の有無に関わらず、最後に必ず実行される処理
			if(br != null) {

				try {

					br.close();//閉じる

				} catch (IOException e) {
					//System.out.println("closeできませんでした");

				}
			}
		}

		File[] files2 = files.listFiles(filter);
		for(int i=0;i<files2.length;i++) {


			int ii = Integer.parseInt(files2[i].getName().substring(0,8));
			if(ii - i != 1){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}

			try {//売上ファイル読込

				FileReader fr2 = new FileReader(files2[i]);
				br2 = new BufferedReader(fr2);


				String shitenCd = br2.readLine();//readLineで1行ずつ読み込む
				String uriage = br2.readLine();


				if (! map1.containsKey(shitenCd))
				{
					System.out.println(files2[i].getName() + "の支店コードが不正です");
					return;
				}



				if(br2.readLine() != null) {
					System.out.println(files2[i].getName() + "のフォーマットが不正です");
					return;
				}

				Long.parseLong(uriage);

				Long sum = map2.get(shitenCd) + Long.parseLong(uriage);

				if(10 < sum.toString().length()) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				map2.put(shitenCd,sum);

			} catch(IOException e) {//例外が発生した場合の処理(Exceptionクラスを指定)
				System.out.println(e);

			}
			finally {
				//例外の有無に関わらず、最後に必ず実行される処理

				if(br != null) {

					try {

						br.close();//閉じる
					} catch (IOException e) {
						//System.out.println("closeできませんでした");
					}
				}
			}


		} File file = new File(args[0],"branch.out");
		BufferedWriter bw;

		try {

			bw = new BufferedWriter(new FileWriter(file));
			for (String key : map2.keySet()) {

				bw.write(key + "," + map1.get(key) + "," + map2.get(key));
				bw.newLine();

			}
			bw.close();

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}
}




